# Install script for directory: F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/dom

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/collada-dom")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "collada-dom2.4-dp-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/collada-dom2.4" TYPE DIRECTORY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/dom/include/1.5" REGEX "/\\.svn$" EXCLUDE REGEX "/\\.\\~$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "collada-dom2.4-dp-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/collada-dom2.4" TYPE DIRECTORY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/dom/include/1.4" REGEX "/\\.svn$" EXCLUDE REGEX "/\\.\\~$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "collada-dom2.4-dp-base")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/Debug/collada-dom2.4-dp-vc100-mt.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/Release/collada-dom2.4-dp-vc100-mt.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/MinSizeRel/collada-dom2.4-dp-vc100-mt.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/RelWithDebInfo/collada-dom2.4-dp-vc100-mt.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "collada-dom2.4-dp-base")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/Debug/collada-dom2.4-dp-vc100-mt.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/Release/collada-dom2.4-dp-vc100-mt.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/MinSizeRel/collada-dom2.4-dp-vc100-mt.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/RelWithDebInfo/collada-dom2.4-dp-vc100-mt.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "collada-dom2.4-dp-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/collada-dom2.4" TYPE DIRECTORY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/dom/include/dae" REGEX "/\\.svn$" EXCLUDE REGEX "/\\.\\~$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "collada-dom2.4-dp-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/collada-dom2.4" TYPE DIRECTORY FILES "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/dom/include/modules" REGEX "/\\.svn$" EXCLUDE REGEX "/\\.\\~$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "collada-dom2.4-dp-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/collada-dom2.4" TYPE FILE FILES
    "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/dom/include/dae.h"
    "F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/dom/include/dom.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/src/1.5/cmake_install.cmake")
  include("F:/Dev/Vs/Deps/collada-dom-2.4.0/collada-dom-2.4.0/collada-dom-2.4.0/build_x64_v140_v4/dom/src/1.4/cmake_install.cmake")

endif()

